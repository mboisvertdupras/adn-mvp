<?php
$_GET['u'] = 'loggedin';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Dragon Nest: Awakening</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/ghast.css">
</head>
<body>

  <main class="wrapper">
    <section class="hero hero-settings text-left">
      <?php include 'partials/navigation.php'; ?>
      <div class="hero-content">
        <div class="container">
          <h1 class="title">Your Account</h1>
          <h2 class="subtitle description">
            Edit your account settings
          </h2>
        </div>
      </div>
    </section>

    <section class="container section pb-0">
      <div class="row">
        <div class="col col-50 col-o-25">
          <div class="card">
            <div class="card-content">
              <h4>Account Details</h4>
              <form action="">
                <fieldset>
                  <label for="acc_name">
                    Change Username:
                    <input type="text" name="acc_name" placeholder="Username">
                  </label>
                  <label for="">
                    Change Email:
                    <input type="email" placeholder="Email">
                  </label>
                  <label for="">
                    Change Password:
                    <input type="password" placeholder="Password">
                  </label>
                  <label for="">
                    <input type="password" placeholder="Repeat Password">
                  </label>
                  <button class="btn green" type="submit" name="button">Save</button>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include 'partials/footer.php'; ?>

  </main>

</body>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" charset="utf-8"></script>
<script src="assets/js/jquery.modal.min.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery(document).ready(function ($) {

		var $toggle = $('#header-toggle');
		var $menu = $('#header-menu');

		$toggle.click(function() {
			$(this).toggleClass('is-active');
			$menu.toggleClass('is-active');
		});

	});
</script>
</html>
