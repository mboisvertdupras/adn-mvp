<div class="hero-header">
  <header class="header">
    <div class="container">
      <div class="header-left">
        <a href="index.php" class="header-tab active">Home</a>
        <a href="#!" class="header-tab">Forums</a>
      </div>
      <span id="header-toggle" class="header-toggle">
        <span></span>
        <span></span>
        <span></span>
      </span>
      <div id="header-menu" class="header-right header-menu">
        <span class="header-item">
          <?php
            if ($_GET['u'] == 'loggedin') {
              ?>
              <span>Welcome back, $user</span>
              <a href="account.php" class="btn">Account Panel</a><?php
            } else {
          ?>
          <span class="header-item"><a href="#" class="btn primary">Download</a></span>
          <div class="btn-group">
            <a href="#login-form" rel="modal:open" class="btn primary inverted outline">Log In</a>
            <a href="#register-form" rel="modal:open" class="btn primary inverted outline">Register</a>
          </div>
          <?php } ?>
        </span>
      </div>
    </div>
  </header>
</div>
