<div class="modal" id="login-form" style="display:none;">
  <h5 class="thin text-center">Login to your Account</h5>
  <form class="mb-0">
    <fieldset class="mb-0">
      <input type="text" placeholder="Username">
      <input type="password" placeholder="Password">
      <div>
        <input type="checkbox" id="confirmField">
        <label class="label-inline" for="confirmField">Remember me</label>
      </div>
      <input class="btn btn-block primary" type="submit" value="Login">
      <small><a href="#register-form" rel="modal:open">Register</a> - <a href="#!">Forgot your password?</a></small>
    </fieldset>
  </form>
  <a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
</div>

<div class="modal" id="register-form" style="display:none;">
  <h5 class="thin text-center">Register a new Account</h5>
  <form class="mb-0">
    <fieldset class="mb-0">
      <input type="text" placeholder="Username">
      <input type="email" placeholder="Email Address">
      <input type="password" placeholder="Password">
      <input type="password" placeholder="Repeat Password">
      <div>
        <input type="checkbox" id="confirmField">
        <small><label class="label-inline" for="confirmField">I read and agree to the <a href="#">Terms and Conditions</a></label></small>
      </div>
      <input class="btn btn-block green" type="submit" value="Register">
      <small><a href="#login-form" rel="modal:open" title="Login to your Account">Already have an account?</a></small>
    </fieldset>
  </form>
  <a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
</div>
